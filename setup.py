import setuptools
import pathlib
import os
import configparser
from pyscripts import build_helper

NAME = 'default'

release = build_helper.Release(os.getenv('RELEASE', 'minor'))
version = build_helper.handle_version(NAME, release=release)

setuptools.setup(
    name='pyscripts',
    version=version.dumps(),
    install_requires=[
        'virtualenv',
        'dataclasses',
    ],
    author='atomikin',
    author_email='atomikin@yandex.ru',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'create-venv=pyscripts.create_env:main',
            'media-converter=pyscripts.media_converter:main',
            'build-helper=pyscripts.build_helper:main',
            'ansible-role=pyscripts.ansible_role:main',
            'docker-build=pyscripts.docker_build:main',
        ]
    },
    scripts=list(
        str(i) for i in
        pathlib.Path(__file__).parent.joinpath('scripts').iterdir()
    )
)

build_helper.handle_version(NAME, save=version)
