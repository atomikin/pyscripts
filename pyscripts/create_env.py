import argparse
import pathlib
import os
import subprocess
import shlex
import sys
import shutil
import tempfile
import typing


def venv_exists(path: pathlib.Path) -> bool:
    if not path.exists():
        return False
    return True


def is_virtualenv_activated():
    return 'VIRTUAL_ENV' in os.environ


def compose_env_path(name: pathlib.Path, version: str) -> pathlib.Path:
    env_path = base_dir().joinpath(
        f'{name}.python{version}'
    )
    return env_path


def base_dir() -> pathlib.Path:
    path = pathlib.Path.home().joinpath('.venvs')
    if not path.exists():
        path.mkdir()
    return path

def find_python(version: str) -> str or pathlib.Path:
    result = subprocess.run(
        shlex.split(f'which python{version}'), stdout=subprocess.PIPE,
    )
    if result.returncode:
        return ''
    for i in result.stdout.decode().splitlines():
        return pathlib.Path(i)


def venv_pip_path(venv_path):
    return venv_path.joinpath('bin', 'pip')


def install_requirements(venv_path, requirements_file):
    if not requirements_file or not requirements_file.stat().st_size:
        return
    if not requirements_file.is_file():
        print(f'file {requirements_file} does not exist', file=sys.stderr)
        return
    pip_path = venv_pip_path(venv_path)
    result = subprocess.run(
        shlex.split(f'{pip_path} install -r {requirements_file}'),
        stdout=subprocess.PIPE
    )
    if result.returncode:
        raise RuntimeError(
            f'Failed to install packages from {requirements_file}'
        )


def install_packages(venv_path, packages):
    if not packages:
        return

    pip_path = venv_pip_path(venv_path)
    packages_str = ' '.join(packages)
    result = subprocess.run(
        shlex.split(f'{pip_path} install {packages_str}'),
        stdout=subprocess.PIPE
    )
    if result.returncode:
        print(
            f'Failed to install packages {packages} '
            f'with {pip_path}, code {result.returncode}',
            file=sys.stderr
        )
        sys.stderr.flush()


def create_venv(env_path: pathlib.Path, interpreter: pathlib.Path) -> int:
    cmd = shlex.split(
        f'virtualenv -p {str(interpreter)} {str(env_path)}'
    )
    res = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
    )
    if res.returncode:
        print(res.stderr.decode(), file=sys.stderr)
    return res.returncode


def collect_current_packages(venv_path: pathlib.Path, fd: typing.TextIO):
    if not venv_path.exists():
        return
    pip_path = venv_pip_path(venv_path)
    res = subprocess.run(shlex.split(f'{pip_path} freeze'), stdout=subprocess.PIPE, check=True)
    fd.write(res.stdout)


def copy_req_file(req_file: typing.Optional[pathlib.Path], fd: typing.TextIO) -> int:
    if not req_file or not req_file.exists():
        return
    with req_file.open() as req_fd:
        fd.write(b'\n')
        fd.write(req_fd.read().encode())
    fd.flush()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--name', '-n', default=pathlib.Path.cwd().absolute().name)
    parser.add_argument('--version', '-V', default='3')
    parser.add_argument('--delete', '-d', action='store_true')
    parser.add_argument('--recreate', '-R', action='store_true')
    parser.add_argument('--delete-only', '-D', action='store_true')
    parser.add_argument('--requirements-file', '-r', type=pathlib.Path)
    parser.add_argument('--packages', '-p', nargs="*", default=())
    args = parser.parse_args()
    if is_virtualenv_activated():
        print(
            f'{os.environ["VIRTUAL_ENV"]} is active. Run "deactivate" first',
            file=sys.stderr
        )
        sys.exit(1)
    interpreter = find_python(args.version)
    if not (interpreter and interpreter.is_file()):
        print(f'"python{args.version}" not found.', file=sys.stderr)
        sys.exit(1)
    env_path = compose_env_path(args.name, args.version)

    if args.delete_only:
        if env_path.is_dir():
            shutil.rmtree(str(env_path))
        return
    with tempfile.NamedTemporaryFile(suffix='.txt') as tmp_file:
        if args.delete and venv_exists(env_path):
            shutil.rmtree(str(env_path))
        elif args.recreate and venv_exists(env_path):
            collect_current_packages(env_path, tmp_file)
            tmp_file.flush()
            shutil.rmtree(str(env_path))

        if not venv_exists(env_path):
            res_code = create_venv(env_path, interpreter)
            if res_code:
                sys.exit(res_code)
        filepath = pathlib.Path(tmp_file.name)
        copy_req_file(args.requirements_file, tmp_file)
        install_requirements(env_path, filepath)
        install_packages(env_path, args.packages)
    print(str(env_path.joinpath('bin', 'activate')))


if __name__ == "__main__":
    main()
