import sys
import enum


class ProgressBar:

    def __init__(self, prefix, width=100, filler='#'):

        self._prefix = Colour.YELLOW.wrap(prefix)
        self._progress = None
        self._width = width
        self._filler = filler

    def init(self):
        line = (
            f'{self._prefix}: '
            f'{Colour.GREEN.wrap("[")}'
            f'{" " * self._width}'
            f'{Colour.GREEN.wrap("]")}'
        )
        self._progress = 0
        sys.stdout.write(line)
        sys.stdout.flush()
        sys.stdout.write('\b' * (self._width + 1))
        sys.stdout.flush()

    def update(self, value: float):
        if self._progress is None:
            self.init()
        current = int(self._width * int(value) / 100)
        if current != self._progress:
            sys.stdout.write(
                Colour.GREEN.wrap(
                    self._filler * (current - self._progress)
                )
            )
            self._progress = current
            sys.stdout.flush()


class Colour(str, enum.Enum):
    RED = '\u001b[31m'
    BLACK = '\u001b[30m'
    GREEN = '\u001b[32m'
    YELLOW = '\u001b[33m'
    BLUE = '\u001b[34m'
    WHITE = '\u001b[37m'
    NC = '\u001b[0m'

    def wrap(self, line: str):
        return f'{self.value}{line}{Colour.NC}'
