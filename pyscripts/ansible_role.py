import pathlib
import argparse
import enum
import sys
import typing


class FolderType(enum.Enum):
    EMPTY = enum.auto()
    NOT_EMPTY = enum.auto()


def handle(roles_dir: pathlib.Path, name: str, flag: FolderType) -> typing.Sequence[pathlib.Path]:
    folder = roles_dir.joinpath(roles_dir, name)
    created_objects = []
    if not folder.is_dir():
        folder.mkdir()
        created_objects.append(folder)
    if flag is FolderType.NOT_EMPTY:
        main_file = folder.joinpath('main.yml')
        if not main_file.exists():
            with main_file.open('w') as f:
                print('---', file=f)
            created_objects.append(main_file)
    return created_objects
            

FOLDERS = {
    'tasks': FolderType.NOT_EMPTY,
    'handlers': FolderType.NOT_EMPTY,
    'meta': FolderType.NOT_EMPTY,
    'vars': FolderType.NOT_EMPTY,
    'templates': FolderType.EMPTY,
    'files': FolderType.EMPTY,
}


def print_result(result: typing.Sequence[pathlib.Path]):
    for i in result:
        obj = 'folder' if i.is_dir() else 'file'
        print(f'{obj} {i} has been created...')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-w', '--work-dir', default=pathlib.Path.cwd(), type=pathlib.Path
    )
    parser.add_argument('-n', '--name', required=True)
    parser.add_argument('-f', '--folder', nargs='*', default=('tasks',))
    args = parser.parse_args()
    roles: pathlib.Path = args.work_dir.joinpath('roles')
    if not roles.is_dir():
        print(f'Path {str(roles)} does not exist', file=sys.stderr)
        sys.exit(1)
    print(f'Creating role {args.name} role in {str(roles)} folder:')
    role = roles.joinpath(args.name)
    if not role.exists():
        role.mkdir()
        print_result([role])
    for i in args.folder:
        if i not in FOLDERS:
            continue
        print_result(handle(role, i, FOLDERS[i]))
    print('done!')


if __name__ == '__main__':
    main()