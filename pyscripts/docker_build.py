#!/usr/bin/env python3
import argparse
import typing
import pathlib
import tempfile
import shutil
import sys
import subprocess
import time
import itertools

ENTRYPOINT = 'entrypoint.sh'
CONFIG = 'docker_config.yml'


def required_tags():
    return ('latest', str(int(time.time())))


def enforce(
        value, 
        err_string,
        exception: Exception = None):
    if value:
        return
    if not exception or not isinstance(exception, Exception):
        exception = Exception
    raise exception(err_string)


class BuildEnv(typing.NamedTuple):
    docker_file: pathlib.Path
    include: typing.List[pathlib.Path]
    include_subpath: pathlib.Path
    build_root: pathlib.Path
    full_image_name: str
    args: typing.Tuple[str]
    registry: str
    tags: typing.List[str]

    def validate(self):
        enforce(
            self.docker_file.is_file(), 
            f'{self.docker_file} not a file', ValueError
        )
        # entrypoint = self.docker_file.parent.joinpath(ENTRYPOINT)
        # enforce(
        #     entrypoint.is_file(), 
        #     f'{entrypoint} not a file', ValueError
        # )
        enforce(
            self.build_root.is_dir(), 
            f'{self.build_root} not a dir', ValueError
        )
        for i in self.include:
            enforce(i.exists(), f'{i} does not exist', ValueError)

    def prepare(self):
        shutil.copytree(self.docker_file.parent, self.build_root, dirs_exist_ok=True)
        for i in self.include:
            target_path = self.build_root.joinpath(self.include_subpath)
            target_path.mkdir(parents=True, exist_ok=True)
            if i.is_dir():
                target_path = target_path.joinpath(i.name)
                target_path.mkdir(exist_ok=True)
                shutil.copytree(i, target_path, dirs_exist_ok=True)
            elif i.is_file():
                shutil.copy(i, target_path)
            else:
                raise ValueError(f'{i} is not file or directory')


class Docker:
    env: BuildEnv

    def __init__(self, build_env: BuildEnv):
        self.env = build_env 

    def _run_cmd(self, cmd: typing.List[str]) -> int:
        print(cmd)
        print('Running command: docker', ' '.join(cmd))
        subprocess.check_call(['docker', *cmd], cwd=self.env.build_root)
        print('Done successlully')

    def build(self):
        cmd = ['build']
        cmd += list(
            itertools.chain(
                *zip(
                    itertools.cycle(['--tag']),
                    (f'{self.env.registry}/{self.env.full_image_name}:{i}' for i in self.env.tags)
                )
            )
        ) 

        cmd += list(itertools.chain(*zip(itertools.cycle(['--build-arg', ]), self.env.args)))
        cmd.append('.')
        self._run_cmd(cmd)

    def push(self):
        for i in self.env.tags:
            self._run_cmd(['push', f'{self.env.registry}/{self.env.full_image_name}:{i}'])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(    
        '-d', '--docker-file', required=True,
        type=pathlib.Path,
        help='Dir contains: Dockerfile, entrypoint, docker_conf.yml'
    )
    parser.add_argument('-i', '--include', nargs='+', default=(), type=pathlib.Path)
    parser.add_argument('-p', '--include-subpath', type=pathlib.Path, default=pathlib.Path('.'))
    parser.add_argument('-r', '--registry', default='192.168.1.58:5000')
    parser.add_argument('-a', '--args', nargs='*', default=())
    parser.add_argument(
        '-t', '--tags', nargs='*', 
        default=()
    )
    parser.add_argument('name')
    args = parser.parse_args()
    with tempfile.TemporaryDirectory() as tmpd:
        env = BuildEnv(
            args.docker_file, args.include, args.include_subpath,
            pathlib.Path(tmpd), args.name, args.args, args.registry,
            tuple(set(required_tags()) | set(args.tags))
        )
        try:
            env.validate()
        except ValueError as err:
            print('Setup error: ', str(err))
            sys.exit(1)
        try:
            env.prepare()
            docker = Docker(env)
            docker.build()
            docker.push()
        except Exception as err:
            print('Unexpected error: ', str(err))
            sys.exit(1)


if __name__ == '__main__':
    main()
