#!/usr/bin/env python

import argparse
import datetime
import enum
import configparser
import pathlib
import typing
import sys

from dataclasses import dataclass

VERSION_FILE = pathlib.Path('version.ini')


@dataclass
class Version:

    major: int = 0
    middle: int = 0
    minor: int = 1

    @classmethod
    def loads(cls, line) -> 'Version':
        a, b, c = line.split('.')
        return cls(int(a), int(b), int(c))

    def dumps(self) -> str:
        return f'{self.major}.{self.middle}.{self.minor}'


class Command(str, enum.Enum):
    VERSION = 'build-version'


class Release(str, enum.Enum):
    MAJOR = 'major'
    MIDDLE = 'middle'
    MINOR = 'minor'

    def __str__(self):
        return self.value


def get_default_conf():
    return {
        'timestamp': datetime.datetime.now().isoformat(),
        'version': Version().dumps(),
    }


def read_version(name):
    if not VERSION_FILE.exists():
        version = Version()
        config = configparser.ConfigParser()
        config[name] = get_default_conf()
        with VERSION_FILE.open('w', encoding='utf-8') as f:
            config.write(f)
        return version
    config = configparser.ConfigParser()
    config.read(str(VERSION_FILE))
    if name not in config:
        config[name] = get_default_conf()
    return Version.loads(config[name]['version'])


def write_version(name: str, version: Version):
    config = configparser.ConfigParser()
    with VERSION_FILE.open('w+', encoding='utf-8') as f:
        config.read(f)
        config[name] = get_default_conf()
        config[name]['version'] = version.dumps()
        f.seek(0)
        config.write(f)


def handle_version(
        name: str, *,
        release: typing.Optional[Release] = None,
        increment: bool = True,
        save: typing.Optional[Version] = None) -> typing.Optional[Version]:
    if save:
        write_version(name, save)
        return
    version = read_version(name)
    if not increment:
        print(version.dumps())
        return
    if release is Release.MINOR:
        version.minor += 1
    elif release is Release.MIDDLE:
        version.minor = 0
        version.middle += 1
    elif release is Release.MAJOR:
        version.minor = 0
        version.middle = 0
        version.major += 1
    return version



def manage_version(args: argparse.Namespace):
    """
    version.ini - file with versions info
    convent:
        version: x.x.x
        build-date: datetime utc
    :return:
    """
    version = handle_version(
        args.name,
        release=args.release,
        increment=args.increment,
        save=args.save
    )
    if version:
        print(version.dumps())


def main(argv: typing.Sequence[str]):
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers()
    version_parser = sub_parsers.add_parser(Command.VERSION.value)
    version_parser.add_argument('-i', '--increment', action='store_true')
    version_parser.add_argument(
        '-r', '--release', choices=Release,
        default=Release.MINOR, type=Release,
    )
    version_parser.add_argument('-s', '--save', type=Version.loads)
    version_parser.add_argument('-n', '--name', default='DEFAULT')
    version_parser.set_defaults(func=manage_version)
    args = parser.parse_args(argv)
    args.func(args)


if __name__ == '__main__':
    main(sys.argv)
