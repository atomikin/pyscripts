import pathlib
import argparse
import re
import asyncio
import sys
from asyncio import subprocess

from . import common

EXECUTABLE = 'HandBrakeCLI'
PROGRESS = re.compile(
    r'Encoding: task \d+ of \d+, (?P<percent>\d+\.\d+) %.*$'
)


async def run(*command) -> subprocess.Process:
    cmd = (EXECUTABLE, *command)
    proc = await asyncio.create_subprocess_exec(
        EXECUTABLE, *cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE
    )
    return proc


def progress_handler(file: pathlib.Path):
    bar = common.ProgressBar(str(file), width=50)

    def func(line):
        match = PROGRESS.match(line.strip())
        if match:
            value = float(match.group('percent'))
            bar.update(value)
    return func


async def read_printer(
        reader: asyncio.StreamReader, handler=None):
    flag = 1
    while flag:
        line = await reader.read(1024)
        line = line.decode().strip('\n')
        flag = not reader.at_eof()
        if not line:
            await asyncio.sleep(1)
            continue
        if not handler:
            print(line, sep='')
            continue
        handler(line)


async def convert_file(
        file: pathlib.Path, dest_folder:pathlib.Path, preset: str):
    name = file.name.rsplit('.', 1)[0]
    dest: pathlib.Path = dest_folder.joinpath(f'{name}.mp4')
    if dest.exists():
        print(
            common.Colour.YELLOW.wrap(f'{str(dest)}: exists, skipping...'),
            end=''
        )
        return
    proc = await run(
        '-i', f'{str(file)}',
        '-o', f'{str(dest)}',
        '-Z', f'{preset}'
    )
    await asyncio.gather(
        read_printer(proc.stdout, handler=progress_handler(dest)),
    )
    if proc.returncode:
        print(
            common.Colour.RED.wrap(
                f'ERROR: {str(file)}: return code {proc.returncode}'
            )
        )


async def app(args: argparse.Namespace):
    reg = re.compile(args.filter_regex)
    files = [i for i in args.folder.iterdir() if reg.match(str(i))]
    files.sort(key=lambda i: i.name)
    for i, file in enumerate(files, start=1):
        print(common.Colour.RED.wrap(f'[{i}/{len(files)}] '), end='')
        await convert_file(file, args.dest, args.preset)
        print()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('folder', type=pathlib.Path)
    parser.add_argument('-f', '--filter-regex', default=r'^.*(mkv)$')
    parser.add_argument('-d', '--dest', required=True, type=pathlib.Path)
    parser.add_argument(
        '-p', '--preset',
        default='Apple 2160p60 4K HEVC Surround'
    )
    args = parser.parse_args()
    if not args.folder.is_dir():
        print(
            common.Colour.RED.wrap(f'{args.folder} does not exit...'),
            file=sys.stderr
        )
        sys.exit(1)
    if not args.dest.is_dir():
        if args.dest.exists():
            print(
                common.Colour.RED.wrap(
                    f'{args.dest} exists and is not a folder'
                )
            )
            sys.exit(1)
        args.dest.mkdir(mode=0o777, parents=True)
    print(common.Colour.GREEN.wrap('\nVideo conversion with HandBrakeCLI...'))
    print(common.Colour.YELLOW.wrap('Info:'))
    print(
        common.Colour.YELLOW.wrap('\tSource:           '),
        str(args.folder)
    )
    print(
        common.Colour.YELLOW.wrap('\tDestination:      '),
        str(args.dest)
    )
    print(
        common.Colour.YELLOW.wrap('\tPreset:           '),
        args.preset
    )
    print(
        common.Colour.YELLOW.wrap('\tExtension filter: '),
        args.filter_regex
    )
    print(
        common.Colour.GREEN.wrap('#' * 20 + ' STARTED ' + '#' * 20 + '\n')
    )
    try:
        asyncio.run(app(args))
    except KeyboardInterrupt:
        print(
            common.Colour.YELLOW.wrap(
                '\nReceived Keyboard Interrupt. Exiting gracefully...'
            )
        )


if __name__ == '__main__':
    main()

